package iesam.tonipons.battleship;

import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.util.Scanner;

public class prova {

	public static void main(String[] args) throws IOException {
		GestorDistribucions gestor = new GestorDistribucions();
		
		int c=0;
		int [] caselles=
			{1,0,3,3,3,
			 0,0,2,2,0,
			 0,0,3,0,0,
			 2,0,3,0,0,
			 2,0,3,0,0};
		
		gestor.setNomFitxer("prova.hlf");
		gestor.setFiles(5);
		gestor.setColumnes(5);
		gestor.setCaselles(caselles);
		int[] camp = new int[gestor.getFiles()*gestor.getColumnes()];
		gestor.guardar();
		camp = gestor.carregar();
		for(int i : camp){
			System.out.print(i);
			c++;
			if(c==gestor.getColumnes()){
				System.out.println();
				c=0;
			}
		}
	}
}
