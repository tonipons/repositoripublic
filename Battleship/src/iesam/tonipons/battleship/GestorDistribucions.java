package iesam.tonipons.battleship;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;


public class GestorDistribucions {
	private int files;
	private int columnes;
	private int[] caselles;
	private String nomFitxer;
	
	
	public String getNomFitxer() {
		return nomFitxer;
	}

	public void setNomFitxer(String nomFitxer) {
		this.nomFitxer = nomFitxer;
	}

	public int getFiles() {
		return files;
	}

	public void setFiles(int files) {
		this.files = files;
	}

	public int getColumnes() {
		return columnes;
	}

	public void setColumnes(int columnes) {
		this.columnes = columnes;
	}
	
	
	public int[] getCaselles() {
		return caselles;
	}

	public void setCaselles(int[] caselles) {
		this.caselles = caselles;
	}

	public void guardar() throws IOException{
		byte [] data = new byte [2];
		FileWriter fw = new FileWriter(nomFitxer);
		fw.write(0);
		fw.write("HLF");
		fw.write(1);
		fw.write(0);
		fw.write("INFO");
		fw.write(4);
		fw.write(0);
		data = escEndian(files);
		fw.write(data[0]);
		fw.write(data[1]);
		data = escEndian(columnes);
		fw.write(data[0]);
		fw.write(data[1]);
		fw.write("VAIX");
		data = escEndian(files*columnes);
		fw.write(data[0]);
		fw.write(data[1]);
		for(int i : caselles){
			fw.write(i);
		}
		fw.write("_FI_");
		fw.write(0);
		fw.write(0);
		fw.close();
	}
	
	public int[] carregar() throws IOException{
		FileInputStream fis = new FileInputStream(nomFitxer);
		byte b1, b2;
		int [] caselles;
		
		fis.skip(12);
		b1 = (byte) fis.read();
		b2 = (byte) fis.read();
		setFiles(convEndian(b1, b2)); 
		
		b1 = (byte) fis.read();
		b2 = (byte) fis.read();
		setColumnes(convEndian(b1, b2));
		
		System.out.println(getFiles()+"-"+getColumnes());
		fis.skip(6);
		
		caselles = new int [getFiles()*getColumnes()];
		
		for(int i=0; i<caselles.length;i++){
			caselles[i] = (byte) fis.read();
		}
		fis.close();
		return caselles;
	}
	
	
	public static int convEndian(int b1, int b2){
		int val = b1 + b2 * 256;
		return val;
	}
	
	public byte[] escEndian(int num){
		byte [] data = new byte [2];
		data[0] = (byte) (num & 0xFF);
		data[1] = (byte) ((num >> 8) & 0xFF);
		return data;
	}
	
	
}
